**Run**

`git clone https://gitlab.com/findersen/docker-laravel`

`cd docker-laravel/deployment`

`docker-compose up -d`

**After container started:**

`cd ../src`

copy .env.example to .env

**Enter to docker bash:**

`docker exec -ti laravel bash`

`composer update`

`php artisan storage:link`

`php artisan migrate --seed`

**Admin panel**: http://localhost:8080/admin
```
login: admin@example.com
password: password
```

**Account**: http://localhost:8080/account
```
login: user1@example.com
password: password
```