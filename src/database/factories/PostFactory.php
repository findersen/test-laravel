<?php

namespace Database\Factories;

use Illuminate\Database\Eloquent\Factories\Factory;
use Illuminate\Support\Str;

class PostFactory extends Factory
{
    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        $images = [
            'https://images.unsplash.com/photo-1619961602105-16fa2a5465c2',
            'https://images.unsplash.com/photo-1520166012956-add9ba0835cb',
            'https://images.unsplash.com/photo-1600783617775-9cb3e067c13b',
            'https://images.unsplash.com/photo-1518400045599-aac8ca22ab02',
            'https://images.unsplash.com/photo-1499314722718-0827eb4deb71',
            'https://images.unsplash.com/photo-1535740560992-3a223ab7ef78',
            'https://images.unsplash.com/photo-1590952186016-798811273a82',
            'https://images.unsplash.com/photo-1554951136-c87e5b6c3228',
            'https://images.unsplash.com/photo-1552847340-1e26a6af19d4',
            'https://images.unsplash.com/photo-1531651008558-ed1740375b39',
        ];

        return [
            'title' => $this->faker->title(),
            'text' => $this->faker->text(250),
            'img' => $images[array_rand($images)],
        ];
    }
}
