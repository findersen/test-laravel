$(function (){
    let $saveBtn = $("#save-btn")
    let $newsImage = $('#image')
    let $newsImageImg = $('#image-img')
    let csrf = $('meta[name="csrf-token"]').attr('content')

    $newsImageImg.attr('src', $newsImage.val())

    $('#generate-image').on('click', function (e){
        e.preventDefault()
        let csrf = $('meta[name="csrf-token"]').attr('content')

        $.ajax({
            type: 'post',
            url: '/admin/news/image',
            headers: {'X-CSRF-Token': csrf},
            cache: false,
            success: function (resp) {
                $newsImageImg.attr('src', resp + '?q=250&w=215')
                $newsImage.val(resp)
            },
            error: function (error) {
                alert(error.message +' ('+ error.status +')');
            }
        });
    })

    $("#edit-post-form").submit(function(e){
        e.preventDefault()
        $saveBtn.attr('disabled', true).html('Please wait...')
        let data = new FormData(this)

        $.ajax({
            type: 'post',
            url: this.action,
            data: data,
            headers: {'X-CSRF-Token': csrf},
            cache: false,
            contentType: false,
            processData: false,
            success: (resp) => {
                if (resp.code === 400) {
                    let message = '<span class="alert alert-danger">'+resp.msg+'</span>'
                    $('#result').html(message)
                } else if (resp.code === 200) {
                    let message = '<span class="alert alert-success">'+resp.msg+'</span>'
                    $('#result').html(message)
                }

                $saveBtn.attr('disabled', false).html('Submit')

                if(resp.redirect) window.location.href = resp.redirect
            },
            error: function (error) {
                alert(error.message +' ('+ error.status +')');
            }
        })
    })
})
