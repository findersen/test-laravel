$(function (){
    let $saveBtn = $("#save-btn")
    let csrf = $('meta[name="csrf-token"]').attr('content')

    $("#avatar_input").change(function(){
        let reader = new FileReader()
        reader.onload = (e) => {
            $("#avatar_img").attr('src', e.target.result)
        }
        reader.readAsDataURL(this.files[0])
    })

    $("#remove-avatar").on('click', function(){
        $("#avatar_img").attr('src', 'https://mdbcdn.b-cdn.net/img/Photos/new-templates/bootstrap-chat/ava6.webp')
        $("#remove_avatar").val(1)
    })

    $("#remove-account").on('click', function(e){
        e.preventDefault()

        if (confirm('Are you sure?')) {
            $.ajax({
                type: 'post',
                url: '/account/destroy',
                headers: {'X-CSRF-Token': csrf},
                cache:false,
                contentType: false,
                processData: false,
                success: (resp) => {
                    if (resp.success) {
                        window.location.href = '/'
                    }
                },
                error: function (error) {
                    alert(error.message +' ('+ error.status +')');
                }
            })
        }
    })

    $("#edit-account-form").submit(function(e){
        e.preventDefault()
        $saveBtn.attr('disabled', true).html('Please wait...')
        let data = new FormData(this)

        $.ajax({
            type: 'post',
            url: this.action,
            data: data,
            headers: {'X-CSRF-Token': csrf},
            cache:false,
            contentType: false,
            processData: false,
            success: (resp) => {
                if (resp.code === 400) {
                    let message = '<span class="alert alert-danger">'+resp.msg+'</span>'
                    $('#result').html(message)
                } else if (resp.code === 200) {
                    let message = '<span class="alert alert-success">'+resp.msg+'</span>'
                    $('#result').html(message)
                }

                $saveBtn.attr('disabled', false).html('Save Profile')
                $("#remove_avatar").val(0)
            },
            error: function (error) {
                alert(error.message +' ('+ error.status +')');
            }
        })
    })
})
