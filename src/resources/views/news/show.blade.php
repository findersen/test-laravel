@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-8">
                @if (session('status'))
                    <div class="alert alert-success" role="alert">
                        {{ session('status') }}
                    </div>
                @endif

                <section>
                    <div class="container py-5">
                        <div class="row justify-content-center">
                            <div class="col-8 text-center">
                                <div class="card mb-5" style="border-radius: 15px; overflow: hidden">
                                    <div class="row g-0" style="background-color: #f4f5f7;">
                                        <div class="col-md-3 gradient-custom text-center text-white"
                                             style="border-top-left-radius: .5rem; border-bottom-left-radius: .5rem;
                                     background-position: center center; background-repeat: no-repeat; background-size: cover;
                                     background-image: url('{{ $item->img }}?q=250&w=215')">
                                        </div>
                                        <div class="col-md-9">
                                            <div class="card-body p-4">
                                                <h3 class="mb-3">{{ $item->title }}</h3>
                                                <p class="small mb-0">
                                                    <i class="far fa-star fa-lg"></i>
                                                    Created on {{ $item->created_at }}
                                                </p>
                                                <hr class="my-4">
                                                <div class="d-flex justify-content-start align-items-center">
                                                    <p>{{ $item->text }}</p>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>

                                <a href="{{ auth()->user()->type ? route('posts') : route('account') }}"
                                   class="btn btn-outline-primary px-3">Go Back</a>
                            </div>
                        </div>
                    </div>
                </section>
            </div>
        </div>
    </div>
@endsection
