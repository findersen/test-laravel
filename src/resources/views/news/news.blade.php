@section('content')
    <section>
        <div class="container py-5">
            <div class="row justify-content-center">
                <div class="col-8">

                    @foreach($list as $item)
                        <div class="card mb-5" style="border-radius: 15px; overflow: hidden">
                            <div class="row g-0" style="background-color: #f4f5f7;">
                                <div class="col-md-3 gradient-custom text-center text-white"
                                     style="border-top-left-radius: .5rem; border-bottom-left-radius: .5rem;
                                     background-position: center center; background-repeat: no-repeat; background-size: cover;
                                     background-image: url('{{ $item->img }}?q=250&w=215')">
                                </div>
                                <div class="col-md-9">
                                    <div class="card-body p-4">
                                        <h3 class="mb-3"><a href="{{ route('account.post', $item->id) }}">{{ $item->title }}</a></h3>
                                        <p class="small mb-0">
                                            <i class="far fa-star fa-lg"></i>
                                            Created on {{ $item->created_at }}
                                        </p>
                                        <hr class="my-4">
                                        <div class="d-flex justify-content-start align-items-center">
                                            <p>{{ \Illuminate\Support\Str::limit($item->text, 240) }}</p>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    @endforeach

                    @if(isset($list))
                        <div class="col-md-12 d-flex justify-content-center">
                            {{ $list->onEachSide(2)->links() }}
                        </div>
                    @endif

                </div>
            </div>
        </div>
    </section>
@endsection
