@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            @if (session('status'))
                <div class="alert alert-success" role="alert">
                    {{ session('status') }}
                </div>
            @endif

            <section>
                <div class="container py-5">
                    <div class="row justify-content-center">
                        <div class="col-8 text-center">
                            <h3>Admin Panel</h3>
                            <a href="{{ route('posts') }}" class="btn btn-primary">News List</a>
                        </div>
                    </div>
                </div>
            </section>
        </div>
    </div>
</div>
@endsection
