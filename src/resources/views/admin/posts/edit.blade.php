@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-8">
                @if (session('status'))
                    <div class="alert alert-success" role="alert">
                        {{ session('status') }}
                    </div>
                @endif

                <section>
                    <div class="container py-5">
                        <div class="row" id="result"></div>
                        <div class="row justify-content-center">
                            <div class="col-10">
                                <h3>Edit: {{ $post->title }}</h3>
                                <div class="row">
                                    <div class="col-12 col-md-3 mt-4">
                                        <img src="{{ $post->img }}?q=250&w=215" alt="" id="image-img" width="135" />
                                        <div class="btn-group mt-3">
                                            <input type="button" id="generate-image" class="btn btn-outline-primary"
                                                   value="Generate Image"/>
                                        </div>
                                    </div>
                                    <div class="col-12 col-md-9">
                                        <form action="{{ route('post.update', $post->id) }}" id="edit-post-form" method="POST">
                                            @csrf
                                            @method('PUT')
                                            <div class="form-group">
                                                <label for="title">Title</label>
                                                <input type="text" name="title" id="title" class="form-control"
                                                       placeholder="Enter Title" value="{{ $post->title }}">
                                            </div>
                                            <div class="form-group">
                                                <label for="img">Image</label>
                                                <input type="text" name="img" id="image" class="form-control" readonly
                                                       value="{{ $post->img }}">
                                            </div>
                                            <div class="form-group">
                                                <label for="text">Text</label>
                                                <textarea name="text" class="form-control" id="text" rows="3">{{ $post->text }}</textarea>
                                            </div>
                                            <div class="mt-3 d-flex justify-content-end">
                                                <a href="{{ route('posts') }}"
                                                   class="btn btn-outline-primary px-3" style="margin-right: 20px">Cancel</a>
                                                <button type="submit" id="save-btn" class="btn btn-primary">Submit</button>
                                            </div>
                                        </form>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </section>

            </div>
        </div>
    </div>
    <script src="{{ asset('js/post.js') }}"></script>
@endsection
