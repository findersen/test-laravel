@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-8">
                @if (session('success'))
                    <div class="alert alert-success" role="alert">
                        {{ session('success') }}
                    </div>
                @endif

                <section>
                    <div class="container py-5">
                        <div class="row" id="result"></div>
                        <div class="row justify-content-center">
                            <div class="col-10">
                                <h3>Create Post</h3>
                                <div class="row">
                                    <div class="col-12 col-md-9">
                                        <form action="{{ route('post.store') }}" id="edit-post-form" method="POST">
                                            @csrf
                                            <div class="form-group">
                                                <label for="title">Title</label>
                                                <input type="text" name="title" id="title" class="form-control"
                                                       placeholder="Enter Title" value="">
                                            </div>
                                            <div class="form-group">
                                                <label for="text">Text</label>
                                                <textarea name="text" class="form-control" id="text" rows="3"></textarea>
                                            </div>
                                            <div class="mt-3 d-flex justify-content-end">
                                                <a href="{{ route('posts') }}"
                                                   class="btn btn-outline-primary px-3" style="margin-right: 20px">Cancel</a>
                                                <button type="submit" id="save-btn" class="btn btn-primary">Submit</button>
                                            </div>
                                        </form>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </section>
            </div>
        </div>
    </div>
    <script src="{{ asset('js/post.js') }}"></script>
@endsection
