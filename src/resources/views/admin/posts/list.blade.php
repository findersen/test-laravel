@extends('layouts.app')

@section('content')
    <section>
        <div class="container py-5">
            <div class="row justify-content-center">
                <div class="col-10">
                    @if (session('success'))
                        <div class="alert alert-success" role="alert">
                            {{ session('success') }}
                        </div>
                    @endif

                    <a href="{{ route('post.create') }}" class="btn btn-outline-primary px-3">New Post</a>

                    <table class="table table-striped">
                        <thead class="thead-dark">
                            <tr>
                                <th scope="col">#</th>
                                <th scope="col">Title</th>
                                <th scope="col">Text</th>
                                <th scope="col">Created at</th>
                                <th scope="col">Actions</th>
                            </tr>
                        </thead>
                        <tbody>
                            @foreach($list as $item)
                                <tr>
                                    <th scope="row">{{ $item->id }}</th>
                                    <td><a href="{{ route('post.show', $item->id) }}">{{ $item->title }}</a></td>
                                    <td>{{ \Illuminate\Support\Str::limit($item->text, 80) }}</td>
                                    <td>{{ $item->created_at }}</td>
                                    <td>
                                        <a href="{{ route('post.edit', $item->id) }}" class="btn btn-outline-success px-3">edit</a>
                                        <form action="{{ route('post.destroy', $item->id) }}" method="POST" type="button" class="btn p-0" onsubmit="return confirm('Delete?')">
                                            @csrf
                                            @method('DELETE')
                                            <button class="btn btn-outline-danger px-3 ">x</button>
                                        </form>
                                    </td>
                                </tr>
                            @endforeach
                        </tbody>
                    </table>

                    @if(isset($list))
                        <div class="col-md-12 d-flex justify-content-center">
                            {{ $list->onEachSide(2)->links() }}
                        </div>
                    @endif

                </div>
            </div>
        </div>
    </section>
@endsection
