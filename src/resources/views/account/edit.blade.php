@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-8">
                @if (session('status'))
                    <div class="alert alert-success" role="alert">
                        {{ session('status') }}
                    </div>
                @endif

                <section>
                    <div class="container py-5">

                        <div class="row" id="result"></div>

                        <form method="POST" enctype="multipart/form-data" id="edit-account-form"
                              action="{{ route('account.update') }}">
                            <input type="hidden" name="remove_avatar" id="remove_avatar" value="0">

                            <div class="row">
                                <div class="col-lg-4">
                                    <div class="card h-100">
                                        <div class="card-body text-center">
                                            <img class="rounded-circle img-fluid" height="150" width="150"
                                                 src="{{ auth()->user()->avatar ? '/storage/'.auth()->user()->avatar : 'https://mdbcdn.b-cdn.net/img/Photos/new-templates/bootstrap-chat/ava6.webp' }}"
                                                 id="avatar_img" />
                                            <div class="d-flex justify-content-center mt-4 mb-2">
                                                <button type="button" id="remove-avatar" class="btn btn-outline-danger px-3">x</button>
                                                <label class="btn btn-outline-primary ms-1">
                                                    Upload Image
                                                    <input type="file" name="avatar" id="avatar_input" style="display: none;">
                                                </label>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-lg-8">
                                    <div class="card h-100">
                                        <div class="card-body">
                                            <div class="row">
                                                <div class="col-12">
                                                    <label for="name" class="labels">Name</label>
                                                    <input type="text" name="name" id="name" class="form-control" placeholder="Enter Your Name" value="{{ auth()->user()->name }}"/>
                                                </div>
                                            </div>
                                            <div class="row mt-3">
                                                <div class="col-12">
                                                    <label for="email" class="labels">Email</label>
                                                    <input type="text" name="email" id="email" class="form-control" placeholder="Enter Email" value="{{ auth()->user()->email }}">
                                                </div>
                                            </div>
                                        </div>
                                        <div class="card-footer">
                                            <div class="d-flex justify-content-between my-2">
                                                <button type="button" id="remove-account" class="btn btn-outline-danger ms-1">Remove Profile</button>
                                                <button type="submit" id="save-btn" class="btn btn-primary">Save Profile</button>
                                            </div>
                                        </div>

                                    </div>
                                </div>
                            </div>
                        </form>

                    </div>
                </section>

            </div>
        </div>
    </div>
    <script src="{{ asset('js/account.js') }}"></script>
@endsection
