<?php

namespace App\Models;

use Illuminate\Http\Request;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use MarkSitko\LaravelUnsplash\Unsplash;

class Post extends Model
{
    use HasFactory, SoftDeletes;

    /**
     * The attributes that are mass assignable.
     *
     * @var array<int, string>
     */
    protected $fillable = [
        'title',
        'text',
        'img',
    ];

    public static function boot()
    {
        parent::boot();

        self::creating(function($model){
            $model->img = self::newImage();
        });
    }

    static public function newImage()
    {
        $randomPhoto = (new Unsplash)->randomPhoto()
            ->orientation('portrait')
            ->term('music')
            ->count(1)
            ->toJson();

        return stristr($randomPhoto[0]->urls->regular, '?', true);
    }
}
