<?php

namespace App\Http\Controllers\Account;

use App\Models\Post;
use App\Services\AccountService;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use Illuminate\Validation\Rule;

class AccountController extends BaseAccountController
{
    private $service;

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct(AccountService $accountService)
    {
        $this->service = $accountService;
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index(Request $request)
    {
        return (new PostController)->index($request);
    }

    public function edit()
    {
        return view('account.edit');
    }

    public function update(Request $request)
    {
        $validated = Validator::make($request->all(), [
            'name' => ['required', 'string', 'max:255'],
            'email' => ['required', 'email', Rule::unique('users')->ignore(auth()->user()->id), 'max:255'],
        ]);

        if ($validated->fails()) {
            return response()->json(['code' => 400, 'msg' => $validated->errors()->first()]);
        }

        $this->service->update($request);

        return response()->json(['code' => 200, 'msg' => 'Profile updated successfully']);
    }

    public function destroy()
    {
        return response()->json([
            'code' => 200,
            'success' => $this->service->destroy()
        ]);
    }

    public function post($id)
    {
        return (new PostController)->show($id);
    }
}
