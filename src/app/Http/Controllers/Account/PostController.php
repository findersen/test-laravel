<?php

namespace App\Http\Controllers\Account;

use App\Models\Post;
use Illuminate\Http\Request;

class PostController extends BaseAccountController
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index(Request $request)
    {
        $list = Post::select()->orderBy('created_at', 'desc')->simplePaginate(5);
        $news = view('news.news', [
            'list' => $list->appends($request->except('page')),
        ]);

        return view('home', compact(['news']));
    }

    public function show($id)
    {
        $item = Post::findOrFail($id);

        return view('news.show', compact('item'));
    }
}
