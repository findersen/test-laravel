<?php

namespace App\Http\Controllers\Admin;

use App\Models\Post;
use Illuminate\Http\Request;
use App\Services\PostService;
use Illuminate\Support\Facades\Validator;

class PostController extends BaseAdminController
{
    private $service;

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct(PostService $postService)
    {
        $this->service = $postService;
        $this->middleware('auth');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index(Request $request)
    {
        $list = Post::select()->orderBy('created_at', 'desc')->simplePaginate(10);

        return view('admin.posts.list', [
            'list' => $list->appends($request->except('page')),
        ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function create()
    {
        return view('admin.posts.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     */
    public function store(Request $request)
    {
        $validated = Validator::make($request->all(), [
            'title' => ['required', 'string', 'max:100'],
            'text' => ['required', 'string', 'max:500'],
        ]);

        if ($validated->fails()) {
            return response()->json(['code' => 400, 'msg' => $validated->errors()->first()]);
        }

        $this->service->store($request->all());
        return response()->json(['code' => 200, 'msg' => 'Post updated successfully', 'redirect' => route('posts')]);
    }

    /**
     * Update the specified resource in storage.
     */
    public function update(Request $request, $id)
    {
        $validated = Validator::make($request->all(), [
            'title' => ['required', 'string', 'max:100'],
            'text' => ['required', 'string', 'max:1000'],
        ]);

        if ($validated->fails()) {
            return response()->json(['code' => 400, 'msg' => $validated->errors()->first()]);
        }

        $this->service->update($id, $request);

        return response()->json(['code' => 200, 'msg' => 'Post updated successfully']);
    }

    /**
     * Display the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function show($id)
    {
        $item = $this->service->getItem($id);

        return view('news.show', compact('item'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function edit($id)
    {
        $post = $this->service->getItem($id);

        return view('admin.posts.edit', compact('post'));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     */
    public function destroy($id)
    {
        $deleted = $this->service->destroy($id);

        return redirect()->route('posts')
            ->with(($deleted ? 'success' : 'danger'), ($deleted ? 'Post removed successfully' : 'Something wrong'));
    }

    public function newImage(Post $post)
    {
        return $post->newImage();
    }
}
