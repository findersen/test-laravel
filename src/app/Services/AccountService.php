<?php
namespace App\Services;

use App\Models\User;
use Illuminate\Support\Facades\File;

class AccountService
{
    public function __construct()
    {
        $this->fields = (new User())->getFillable();
    }

    public function update($request): User
    {
        if ($request->hasFile('avatar')) {
            $imagePath = 'storage/'.auth()->user()->avatar;
            if (File::exists($imagePath)) {
                File::delete($imagePath);
            }
            $avatar = $request->avatar->store('avatar', 'public');
        }

        $update = [
            'name' => $request->name,
            'email' => $request->email,
        ];

        if(! empty($avatar)) $update['avatar'] = $avatar;
        if($request->remove_avatar) $update['avatar'] = null;

        $user = User::findOrFail(auth()->user()->id);
        $user->update($update);

        return $user;
    }

    public function getItem($id): User
    {
        return User::findOrFail($id);
    }

    public function destroy() :bool
    {
        return User::whereId(auth()->user()->id)->delete();
    }
}
