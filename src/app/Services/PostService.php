<?php
namespace App\Services;

use App\Models\Post;

class PostService
{
    public function __construct()
    {
        $this->fields = (new Post())->getFillable();
    }

    public function store(array $postData): Post
    {
        $post = Post::create($postData);
        return $post;
    }

    public function update($id, $request): Post
    {
        $post = Post::findOrFail($id);
        $post->update($request->only($this->fields));

        return $post;
    }

    public function getItem($id): Post
    {
        return Post::findOrFail($id);
    }

    public function destroy($id) :bool
    {
        return Post::whereId($id)->delete();
    }
}
