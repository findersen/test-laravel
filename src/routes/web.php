<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Auth::routes();

Route::middleware('auth')->group(function () {
    Route::get('admin', function () {
        return view('admin.home');
    })->middleware('is.admin')->name('admin');

    Route::controller(App\Http\Controllers\Admin\PostController::class)
        ->prefix('admin/news')->middleware('is.admin')->group(function ()
    {
        Route::get('', 'index')->name('posts');
        Route::get('create', 'create')->name('post.create');
        Route::post('store', 'store')->name('post.store');
        Route::get('show/{id}', 'show')->name('post.show');
        Route::get('edit/{id}', 'edit')->name('post.edit');
        Route::put('edit/{id}', 'update')->name('post.update');
        Route::delete('destroy/{id}', 'destroy')->name('post.destroy');
        Route::post('image', 'newImage')->name('new.image');
    });

    Route::controller(App\Http\Controllers\Account\AccountController::class)->prefix('account')->group(function () {
        Route::get('', 'index')->name('account')->middleware('is.user');
        Route::get('edit', 'edit')->name('account.edit');
        Route::get('post/{id}', 'post')->name('account.post');
        Route::post('update', 'update')->name('account.update');
        Route::post('destroy', 'destroy')->name('account.destroy');
    });
});
